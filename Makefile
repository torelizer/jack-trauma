CC=gcc

LD_OPTS=-ljack 

CC_OPTS=-O3 -Wall

EXECS=jack_trauma 

all:	$(EXECS) $(MODULES)

jack_trauma: jack_trauma.c
	$(CC) -o $@ $@.c $(LD_OPTS)

clean:
	rm -f $(EXECS)

